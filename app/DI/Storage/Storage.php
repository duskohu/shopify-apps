<?php

namespace App\DI\Storage;

use Nette\Caching\Cache;
use Nette\Caching\Storages\FileStorage;
use Nette\Utils\FileSystem;
use Nette\Utils\Strings;

class Storage
{
    /** @var  string */
    protected $dataPath;
    
    /** @var  string */
    protected $shop;
    
    /** @var  Cache */
    protected $cache;
    
    /**
     * @param string $dataPath
     * @param string $shop
     */
    public function __construct($dataPath, $shop)
    {
        $this->dataPath = $dataPath;
        $this->shop = $shop;
        
        $dataDir = $this->getDataDir();
        $storage = new FileStorage($dataDir);
        $this->cache = new Cache($storage, 'data_storage');
    }
    
    /**
     * @return string
     */
    public function getDataDir()
    {
        $path = $this->dataPath . DIRECTORY_SEPARATOR . Strings::webalize($this->shop);
        if (!is_dir($path)) {
            FileSystem::createDir($path);
        }
        return $path;
    }
    
    /**
     * @param string $key
     * @param mixed $value
     * @return mixed
     * @throws \Throwable
     */
    public function save($key, $value)
    {
        return $this->cache->save($key, $value);
    }
    
    /**
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->cache->load($key);
    }
    
    /**
     * @return $this
     */
    public function clear()
    {
        $this->cache->clean([Cache::ALL => true]);
        return $this;
    }
}
