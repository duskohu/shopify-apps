<?php

namespace App\DI\OAuth;

class ShopifyOauthFactory
{
    /** @var  string */
    protected $dataPath;

    /**
     * @param string $dataPath
     */
    public function __construct($dataPath)
    {
        $this->dataPath = $dataPath;
    }

    /**
     * @param string $apiKey
     * @param string $sharedSecret
     * @param string $appName
     * @return ShopifyOauth
     */
    public function create($apiKey, $sharedSecret, $appName)
    {
        return new ShopifyOauth($apiKey, $sharedSecret, $appName, $this->dataPath);
    }
}
