#!/bin/bash
set -e

if [[ -z $1 ]]; then
    XUID=1000
else
    XUID=$1
fi

apt-get update
apt-get install -y sudo libc-client2007e

usermod --non-unique --uid $XUID www-data
usermod --non-unique --uid $XUID application

chown -R application /tmp
chown -R application /var/www
apache2ctl -k graceful
service php-fpm restart
