<?php

namespace App\Model;

use App\DI\Storage\Storage;
use App\DI\Storage\StorageFactory;
use Nette\Utils\Arrays;
use Slince\Shopify\Client;
use Slince\Shopify\Manager\Product\Product;
use Slince\Shopify\PublicAppCredential;

class Products
{
    const  PRODUCTS_IDS_KEY = 'products_ids';
    
    /** @var StorageFactory */
    private $storageFactory;
    
    /**
     * @param StorageFactory $storageFactory
     */
    public function __construct(StorageFactory $storageFactory)
    {
        $this->storageFactory = $storageFactory;
    }
    
    /**
     * @param string $shop
     * @return Storage
     */
    protected function getSQLiteJournalStorage($shop)
    {
        return $this->storageFactory->create($shop);
    }
    
    /**
     * @param string $shop
     * @param string $accessToken
     * @param array $query
     * @param array $excludeTags
     * @return Product[]
     * @throws \Throwable
     */
    public function downloadProducts($shop, $accessToken, $query = [], $excludeTags = [])
    {
        $storage = $this->getSQLiteJournalStorage($shop);
        
        $credential = new PublicAppCredential($accessToken);
        $client = new Client($credential, $shop, [
            'metaCacheDir' => $storage->getDataDir() . DIRECTORY_SEPARATOR . '/tmp' // Metadata cache dir, required
        ]);
        
        $limit = 100;
        $productsCount = $client->getProductManager()->count($query);
        $pages = ceil($productsCount / $limit);
        
        $query['limit'] = $limit;
        $newProducts = [];
        $storage->clear();
        
        for ($i = 1; $i <= $pages; $i++) {
            $query['page'] = $i;
            /** @var Product[] $products */
            $products = $client->getProductManager()->findAll($query);
            foreach ($products as $product) {
                $tagsStr = $product->getTags();
                $tags = [];
                if (!empty($tagsStr)) {
                    $tags = explode(',', $tagsStr);
                }
                
                $res = Arrays::some($tags, function ($value) use ($excludeTags) {
                    return in_array(trim($value), $excludeTags);
                });
                
                if ($res == false) {
                    $storage->save($product->getId(), $product);
                    $newProducts[] = $product->getId();
                }
            }
        }
        
        $storage->save(self::PRODUCTS_IDS_KEY, $newProducts);
        return $newProducts;
    }
    
    /**
     * @param string $shop
     * @return Product[]
     */
    public function getProducts($shop)
    {
        $storage = $this->getSQLiteJournalStorage($shop);
        $storage = $storage->get('products');
        return $storage ? $storage : [];
    }
    
    /**
     * @param string $shop
     * @return array
     */
    public function getProductsIds($shop)
    {
        $storage = $this->getSQLiteJournalStorage($shop);
        $storage = $storage->get(self::PRODUCTS_IDS_KEY);
        return $storage ? $storage : [];
    }
    
    /**
     * @param string $shop
     * @param int $id
     * @return Product|null
     */
    public function getProduct($shop, $id)
    {
        $storage = $this->getSQLiteJournalStorage($shop);
        $product = $storage->get($id);
        return $product ? $product : null;
    }
    
}
