##INSTALLATION INSTRUCTION

For install project by Docker, you must correct install docker-compose package. More info about the installation is at url: [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

***Before start installation stop all XDebug listeners !!!***

#1. Create custom docker config
- create dir `<PROJECTDIR>/../docker`
- copy `env.default` to `<PROJECTDIR>/../docker/.env` and rename to `.env`
- channge path in `<PROJECTDIR>/../docker/.env` to `<PROJECTDIR>`
- copy `docker-compose.yml` to `<PROJECTDIR>/../docker/docker-compose.yml`
- add to `<PROJECTDIR>/../docker/docker-compose.yml` config in to `mariadb` > `user: "1000"` where id get from local server `$ id -u`
```
    mariadb:
        ...
        user: "1000"
```
- copy `.ssh` to `<PROJECTDIR>/../docker/.ssh`
- copy existing rsa key or create new to `<PROJECTDIR>/../docker/.ssh`
- set roles for `<PROJECTDIR>/../docker/.ssh` to:
```
chmod  700 .ssh/
chmod 600 .ssh/*
```
- for generate ssh [use this manual](https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/)
- add your ssh to bitbucket project

#2. Create Docker containers
Execute next command:

`docker-compose -p shopifyapps up` - first start

`docker-compose -p shopifyapps up -d` - deamon start

***After every up run poin 3 for corect rules!!!***

Now you have prepared environment for your new project. In the next step, you can update database and download custom data from production server. 

#3. Run start.sh 
`docker-compose -p shopifyapps exec web-server /installation/start.sh`
or when you have another UID as 1000 use
`docker-compose -p shopifyapps exec web-server /installation/start.sh 666`

For corect rules

#6. Composer update
`docker-compose -p shopifyapps exec -u www-data web-server /bin/bash -c "cd /var/www/html && composer update"`

#7. Work
- list of docker containers `docker ps`
- start docker `docker-compose -p shopifyapps up -d && docker-compose -p shopifyapps exec web-server /installation/start.sh` 
- stop docker `docker-compose -p shopifyapps down` after start you must run point 3 for corect rules
- go to docker container `docker-compose -p shopifyapps exec -u www-data web-server /bin/bash` as www-data user

#8. Config
- Copy `install/config.local.neon` to `<PROJECTDIR>/app/config/config.local.neon`
- Run migrations for create and update DB`docker-compose -p shopifyapps exec -u www-data web-server /bin/bash -c "php /var/www/html/bin/console migrations:continue"`
- Create new user in DB

