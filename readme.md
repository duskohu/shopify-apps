Shopify Apps Sandbox
=============

Installation
------------

The best way to install Web Project is using Composer.

	git clone git@bitbucket.org:duskohu/shopify-apps.git
	cd shopify-apps
	composer update


Make directories `temp/`, `log/` and `app/data` writable.


Requirements
------------

- PHP 7.1


Configure
----------------------------

Create `app/config/config.local.neon`:

```neon
parameters:
	randomProducts:
		apiKey: 'YourAppApiKey'
		sharedSecret: 'YourAppSharedSecret'
```

Usage
----------------------------
####RandomProducts:
- Register shop: `/random-products`
- Download products to storage: `/random-products/download-products?shop=your-shop.myshopify.com`
- Download products to storage with exclude-tags: `/random-products/download-products?shop=your-shop.myshopify.com`&exclude-tags[]=value1&exclude-tags[]=pfs:hidden
- Load products: `/random-products/get?shop=your-shop.myshopify.com[&count=10]`, default count is 15
- **Partners shopify app setup:**
    - Install url: `/random-products/register`
    - Whitelisted redirection URL(s): `/random-products/generate-token`
    
**Add to www-data user crontab following command:**

`/1 * /bin/curl https://project-domain.com/random-products/download-products?your-shop.myshopify.com`

