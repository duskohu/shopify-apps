<?php

namespace App\Presenters;

use App\DI\OAuth\ShopifyOauth;
use Nette\Application\BadRequestException;
use Nette\Application\Responses\JsonResponse;
use Nette\Application\UI\Form;
use Nette\Application\UI\Presenter;
use Nette\Utils\Arrays;
use Nette\Utils\Strings;


final class RandomProductsPresenter extends Presenter
{
    const APP_NAME = 'random-products';
    
    /** @var array */
    protected $scopes = ['read_products', 'write_products'];
    
    /** @var string */
    protected $apiKey;
    
    /** @var string */
    protected $sharedSecret;
    
    /** @var  ShopifyOauth */
    protected $shopifyOauth;
    
    /** @var \App\DI\OAuth\ShopifyOauthFactory @inject */
    public $shopifyOauthFactory;
    
    /** @var \App\Model\Products @inject */
    public $products;
    
    /** @const HTTP_METHOD_OPTIONS */
    const HTTP_METHOD_OPTIONS = 'OPTIONS';
    
    public function startup()
    {
        parent::startup();
        $this->shopifyOauth = $this->shopifyOauthFactory->create($this->apiKey, $this->sharedSecret, self::APP_NAME);
    }
    
    /**
     * @param string $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }
    
    /**
     * @param string $sharedSecret
     */
    public function setSharedSecret($sharedSecret)
    {
        $this->sharedSecret = $sharedSecret;
    }
    
    /**
     * @param string $shop
     * @throws \Nette\Application\AbortException
     * @throws \Nette\Application\UI\InvalidLinkException
     */
    public function actionRegister($shop)
    {
        $redirectUri = $this->link('//:RandomProducts:generateToken');
        $installUrl = $this->shopifyOauth->getInstallUrl($shop, $this->scopes, $redirectUri);
        $this->redirectUrl($installUrl->getAbsoluteUrl());
    }
    
    public function actionGenerateToken()
    {
        $generateToken = $this->shopifyOauth->generateToken(
            $this->request->getParameter('hmac'),
            $this->request->getParameter('code'),
            $this->request->getParameter('shop'),
            $this->request->getParameter('timestamp')
        );
        
        if ($generateToken) {
            $this->flashMessage('Access added', 'success');
        } else {
            $this->flashMessage('This request is NOT from Shopify!', 'success');
        }
        $this->redirect('default');
    }
    
    /**
     * @return Form
     */
    public function createComponentRegister()
    {
        $form = new Form();
        $form->addText('shop', 'Shop: https://')
            ->addRule($form::FILLED, 'The "%label" field must be filled in!');
        $form->addSubmit('send', 'Register');
        $form->onSuccess[] = function (Form $form, $values) {
            $this->redirect('register', ['shop' => 'https://' . $values->shop]);
        };
        return $form;
    }
    
    /**
     * @throws BadRequestException
     * @throws \Nette\Application\AbortException
     * @throws \Throwable
     */
    public function actionDownloadProducts()
    {
        $shop = $this->request->getParameter('shop');
        if (!$shop) {
            throw new BadRequestException('Shop is not defined!');
        }
        
        $excludeTags = $this->request->getParameter('exclude-tags');
        if (!$excludeTags || !is_array($excludeTags)) {
            $excludeTags = [];
        }
        
        $webalizeShop = Strings::webalize($shop);
        $accessToken = $this->shopifyOauth->getAccessToken($webalizeShop);
        
        $query = ['published_status' => 'published'];
        $products = $this->products->downloadProducts($shop, $accessToken, $query, $excludeTags);
        
        $queryOutput = implode(', ', array_map(
            function ($v, $k) {
                return sprintf("%s='%s'", $k, $v);
            },
            $query,
            array_keys($query)
        ));
      
        echo 'Downloaded: ' . count($products) . ' products | Query: ' . $queryOutput . ' | Exclude tags: ' . implode(', ',
                $excludeTags);
        //bdump($products);
        $this->terminate();
    }
    
    /**
     * @throws BadRequestException
     * @throws \Nette\Application\AbortException
     */
    public function actionGet()
    {
        $shop = $this->request->getParameter('shop');
        if (!$shop) {
            throw new BadRequestException('Shop is not defined!');
        }
        $count = $this->request->getParameter('count');
        if (!$count) {
            $count = 15;
        }
        
        $response = $this->getHttpResponse();
        $response->setHeader('Pragma', null);
        $this->enableCors();
        $response->setExpiration('0sec');
        
        $method = $this->request->getMethod();
        if ($method == static::HTTP_METHOD_OPTIONS) {
            $this->terminate();
        }
        
        $data = [];
        $products = $this->products->getProductsIds($shop);
        
        if (!empty($products)) {
            
            $randomProductsIds = array_rand($products, $count);
            if ($count == 1) {
                $randomProducts[] = $randomProductsIds;
            } else {
                $randomProducts = $randomProductsIds;
            }
            
            $randomProductsIds = array_intersect_key($products, array_flip($randomProducts));
            
            foreach ($randomProductsIds as $id) {
                $product = $this->products->getProduct($shop, $id);
                $data[$id] = $this->object_to_array($product);
            }
        }
        
        $this->sendResponse(new JsonResponse($data, 'application/json;charset=utf-8'));
    }
    
    /**
     * @throws BadRequestException
     */
    public function actionLoad()
    {
        throw new BadRequestException('Not supported!');
    }
    
    private function enableCors()
    {
        $response = $this->getHttpResponse();
        $response->setHeader('Access-Control-Allow-Origin', '*');
        $response->setHeader('Access-Control-Allow-Credentials', 'false');
        $response->setHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        $response->setHeader('Access-Control-Allow-Headers', 'Content-Type');
        $response->setHeader('Access-Control-Max-Age', '0');
    }
    
    public function object_to_array($obj)
    {
        if (is_object($obj)) {
            $obj = (array)$this->dismount($obj);
        }
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = $this->object_to_array($val);
            }
        } else {
            $new = $obj;
        }
        return $new;
    }
    
    public function dismount($object)
    {
        $reflectionClass = new \ReflectionClass(get_class($object));
        $array = array();
        foreach ($reflectionClass->getProperties() as $property) {
            $property->setAccessible(true);
            $array[$property->getName()] = $property->getValue($object);
            $property->setAccessible(false);
        }
        return $array;
    }
    
}
