<?php

namespace App\DI\Storage;

class StorageFactory
{
    /** @var  string */
    protected $dataPath;

    /**
     * @param string $dataPath
     */
    public function __construct($dataPath)
    {
        $this->dataPath = $dataPath;
    }

    /**
     * @param string $shop
     * @return Storage
     */
    public function create($shop)
    {
        return new Storage($this->dataPath, $shop);
    }
}
