<?php

namespace App\DI\OAuth;

use Nette\Http\Url;
use Nette\Utils\FileSystem;
use Nette\Utils\Strings;

class ShopifyOauth
{
    const TOKEN_FILE = 'token';

    /** @var string */
    protected $apiKey;

    /** @var string */
    protected $sharedSecret;

    /** @var  string */
    protected $appName;

    /** @var  string */
    protected $dataPath;

    /**
     * @param string $apiKey
     * @param string $sharedSecret
     * @param string $appName
     * @param string $dataPath
     */
    public function __construct($apiKey, $sharedSecret, $appName, $dataPath)
    {
        $this->apiKey = $apiKey;
        $this->appName = $appName;
        $this->dataPath = $dataPath;
        $this->sharedSecret = $sharedSecret;
    }

    /**
     * @param string $shop
     * @return string
     */
    public function getDataDir($shop)
    {
        $path = $this->dataPath . DIRECTORY_SEPARATOR . Strings::webalize($shop) . DIRECTORY_SEPARATOR . $this->appName;
        if (!is_dir($path)) {
            FileSystem::createDir($path);
        }
        return $path;
    }

    /**
     * @param string $shop
     * @param array $scopes
     * @param string $redirectUri
     * @return Url
     */
    public function getInstallUrl($shop, $scopes = array(), $redirectUri)
    {
        $url = new Url();
        $url->setPath($shop . '/admin/oauth/authorize');
        $url->setQueryParameter('client_id', $this->apiKey);
        $url->setQueryParameter('scope', implode(',', $scopes));
        $url->setQueryParameter('redirect_uri', $redirectUri);
        return $url;
    }

    /**
     * @param string $hmac
     * @param string $code
     * @param string $shop
     * @param string $timestamp
     * @return bool
     */
    public function generateToken($hmac, $code, $shop, $timestamp)
    {
        $params = [
            'code' => $code,
            'shop' => $shop,
            'timestamp' => $timestamp,
        ];

        ksort($params); // Sort params lexographically
        $computedHmac = hash_hmac('sha256', http_build_query($params), $this->sharedSecret);
        // Use hmac data to check that the response is from Shopify or not
        if (hash_equals($hmac, $computedHmac)) {
            $query = array(
                "client_id" => $this->apiKey, // Your API key
                "client_secret" => $this->sharedSecret, // Your app credentials (secret key)
                "code" => $code // Grab the access key from the URL
            );

            // Generate access token URL
            $accessTokenUrl = "https://" . $shop . "/admin/oauth/access_token";

            // Configure curl client and execute request
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $accessTokenUrl);
            curl_setopt($ch, CURLOPT_POST, count($query));
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
            $result = curl_exec($ch);
            curl_close($ch);

            // Store the access token
            $result = json_decode($result, true);
            $accessToken = $result['access_token'];

            $this->saveAccessToken($shop, $accessToken);
            return true;


        } else {
            return false;
        }
    }

    /**
     * @param string $shop
     * @param string $accessToken
     */
    public function saveAccessToken($shop, $accessToken)
    {
        FileSystem::write($this->getDataDir($shop) . DIRECTORY_SEPARATOR . self::TOKEN_FILE, $accessToken);
    }

    /**
     * @param string $shop
     * @return string
     */
    public function getAccessToken($shop)
    {
        return FileSystem::read($this->getDataDir($shop) . DIRECTORY_SEPARATOR . self::TOKEN_FILE);
    }

}
